//includes
#include "../libs/include.h"




//variaveis globais
int x = 0;
pthread_mutex_t x_mutex;
pthread_cond_t x_cond1;
pthread_cond_t x_cond2;
#define NTHREADS  5




//thread 1
void *t5 (void *arg) {
    printf("Seja bem-vindo!\n");
    pthread_mutex_lock(&x_mutex);
    x++;
    pthread_cond_broadcast(&x_cond1); //abre as threads 2, 3 ou 4 que estiverem esperando

    pthread_mutex_unlock(&x_mutex); 


    pthread_exit(NULL);
}




//thread 2
void *t2 (void *arg) {

    pthread_mutex_lock(&x_mutex);
    if(x<1){ //espera a thread 1
        pthread_cond_wait(&x_cond1, &x_mutex);
    }
    printf("Fique a vontade.\n");
    x++;
    if(x==4) pthread_cond_signal(&x_cond2); //abre a thread 5

    pthread_mutex_unlock(&x_mutex); 


    pthread_exit(NULL);
}




//thread 3
void *t3 (void *arg) {
    
    pthread_mutex_lock(&x_mutex);
    if(x<1){ //espera a thread 1
        pthread_cond_wait(&x_cond1, &x_mutex);
    }
    printf("Sente-se por favor.\n");
    x++;
    if(x==4) pthread_cond_broadcast(&x_cond2); //abre a thread 5

    pthread_mutex_unlock(&x_mutex); 


    pthread_exit(NULL);
}




//thread 4
void *t4 (void *arg) {

    pthread_mutex_lock(&x_mutex);
    if(x<1){ //espera a thread 1
        pthread_cond_wait(&x_cond1, &x_mutex);
    }
    printf("Aceita um copo d’agua?\n");
    x++;
    if(x==4) pthread_cond_broadcast(&x_cond2); //abre a thread 5

    pthread_mutex_unlock(&x_mutex); 


    pthread_exit(NULL);
}




//thread 5
void *t1 (void *arg) {

    pthread_mutex_lock(&x_mutex);
    if(x<4){
        pthread_cond_wait(&x_cond2, &x_mutex);
    }

    printf("Volte sempre!\n");
    
    pthread_mutex_unlock(&x_mutex); 


    pthread_exit(NULL);
}










//main
int main(int argc, char *argv[]) {

    int i; 
    pthread_t threads[NTHREADS];


    /* Inicilaiza o mutex (lock de exclusao mutua) e as variaveis de condicao */
    pthread_mutex_init(&x_mutex, NULL);
    pthread_cond_init (&x_cond1, NULL);
    pthread_cond_init (&x_cond2, NULL);


    /* Cria as threads */
    pthread_create(&threads[0], NULL, t1, NULL);
    pthread_create(&threads[1], NULL, t2, NULL);
    pthread_create(&threads[2], NULL, t3, NULL);
    pthread_create(&threads[3], NULL, t4, NULL);
    pthread_create(&threads[4], NULL, t5, NULL);



    /* Espera todas as threads completarem */
    for (i = 0; i < NTHREADS; i++) {
        pthread_join(threads[i], NULL);
    }



    /* Desaloca variaveis e termina */
    pthread_mutex_destroy(&x_mutex);
    pthread_cond_destroy(&x_cond1);
    pthread_cond_destroy(&x_cond2);



    return 0;
}
