# Computação Concorrente

## Laboratório 5 - atividade 4:  Programa de boas vindas

<br>

### Michel Monteiro Schorr 120017379

<br><br>

### Dentro da pasta lab, irá encontrar 3 pastas:  
**execs**, contendo os executaveis  
**libs**, contendo as bibliotecas, macros, funcoes, estruturas, etc  
**src**, contendo o condigo fonte principal

<br><br>

## Comandos

Os comandos para compilar e executar são os seguintes:
<br>
<br>


``` 
gcc ./lab/src/ativ4.c -o ./lab/execs/ativ4 -Wall -lpthread -lm
./lab/execs/ativ4
```
<br><br><br><br>
